require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:taxon)             { create(:taxon) }
  let(:product)           { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  it "related_productsメソッドで関連商品が取得される" do
    expect(product.related_products).to be_truthy
    expect(product.related_products).to include related_products[0]
  end

  it "関連商品に重複する商品は含まれない" do
    expect(related_products == related_products.uniq).to be_truthy
  end

  it "関連商品にメインの商品は含まれない" do
    expect(product.related_products).not_to include product
  end
end
