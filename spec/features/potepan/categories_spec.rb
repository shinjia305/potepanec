require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxonomy)  { create(:taxonomy) }
  let!(:taxon1)   { create(:taxon, taxonomy: taxonomy) }
  let!(:taxon2)   { create(:taxon, taxonomy: taxonomy) }
  let!(:product)  { create(:product, taxons: [taxon1]) }

  before do
    visit potepan_category_path(taxon1.id)
  end

  it "ページタイトルが動的に表示される" do
    within ".page-title" do
      expect(page).to have_content taxon1.name
    end
  end

  it "サイドバーにカテゴリーが表示される" do
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content "#{taxon1.name}(#{taxon1.products.count})"
      expect(page).to have_content "#{taxon2.name}(#{taxon2.products.count})"
    end
  end

  it "選択したカテゴリーの商品が表示される" do
    within ".side-nav" do
      click_link "#{taxon1.name}(#{taxon1.products.count})"
    end
    expect(current_path).to eq potepan_category_path(taxon1.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end

  it "選択していないカテゴリーの商品は表示されない" do
    within ".side-nav" do
      click_link "#{taxon2.name}(#{taxon2.products.count})"
    end
    expect(current_path).to eq potepan_category_path(taxon2.id)
    expect(page).not_to have_content product.name
    expect(page).not_to have_content product.display_price
  end

  it "商品名クリックで、商品詳細ページが表示される" do
    within ".productBox" do
      click_link product.name
    end
    expect(current_path).to eq potepan_product_path(product.id)
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
  end
end
