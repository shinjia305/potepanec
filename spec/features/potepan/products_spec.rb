require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  include ApplicationHelper
  let(:taxonomy)           { create(:taxonomy) }
  let(:taxon)              { create(:taxon, taxonomy: taxonomy) }
  let(:taxon2)             { create(:taxon, taxonomy: taxonomy) }
  let(:product)            { create(:product, taxons: [taxon]) }
  let!(:related_products)  { create_list(:product, 5, taxons: [taxon]) }
  let!(:related_products2) { create(:product, taxons: [taxon2]) }

  before do
    visit potepan_product_path(product.id)
  end

  it "商品詳細ページのタイトルに商品名が表示される" do
    expect(page).to have_title "#{product.name} | BIGBAG Store"
    within ".page-title" do
      expect(page).to have_content product.name
    end
  end

  it "商品名・価格・説明が表示される" do
    within ".singleProduct" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  it "一覧ページへ戻るをクリックするとカテゴリーページへ遷移する" do
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
    expect(page).to have_content taxonomy.name
    expect(page).to have_content taxon.name
  end

  it "関連商品が表示される" do
    within ".productsContent" do
      expect(page).to have_content related_products[0].name
      expect(page).to have_content related_products[0].display_price
    end
  end

  it "関連しない商品は表示されない" do
    within ".productsContent" do
      expect(page).not_to have_content related_products2.name
    end
  end

  it "関連商品が4件表示される" do
    within ".productsContent" do
      expect(page).to have_selector ".productBox", count: 4
    end
  end

  it "関連商品をクリックすると商品詳細ページへ遷移する" do
    within ".productsContent" do
      click_on related_products[0].name
    end
    expect(current_path).to eq potepan_product_path(related_products[0].id)
  end
end
