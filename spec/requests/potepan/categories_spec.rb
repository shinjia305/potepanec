require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /potepan/categories" do
    let(:taxonomy)  { create(:taxonomy) }
    let!(:taxon1)   { create(:taxon, taxonomy: taxonomy) }
    let!(:taxon2)   { create(:taxon, taxonomy: taxonomy) }
    let!(:product)  { create(:product, taxons: [taxon1]) }

    before do
      get potepan_category_path(taxon1.id)
    end

    it "カテゴリーページが表示される" do
      expect(response).to have_http_status(200)
    end

    it "カテゴリーページにShowテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@taxonomy、@taxon、@productが表示される" do
      expect(response.body).to include "#{taxonomy.name}"
      expect(response.body).to include "#{taxon1.name}"
      expect(response.body).to include "#{product.name}"
    end

    it "選択されたtaxonに関連する商品が表示される" do
      expect(response.body).to include product.name
    end

    it "選択されていないtaxonに関連する商品は表示されない" do
      get potepan_category_path(taxon2.id)
      expect(response.body).not_to include product.name
    end
  end
end
