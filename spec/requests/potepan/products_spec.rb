require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe "GET /potepan/products" do
    let(:taxon)             { create(:taxon) }
    let(:product)           { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "商品詳細ページが表示される" do
      expect(response).to have_http_status(200)
    end

    it "商品詳細ページにShowテンプレートが表示される" do
      expect(response).to render_template :show
    end

    it "@product、@related_productsが表示される" do
      expect(response.body).to include "#{product.name}"
      expect(response.body).to include "#{related_products[0].name}"
    end

    it "関連商品が4件取得される" do
      expect(Capybara.string(response.body)).to have_selector ".productBox", count: 4
    end
  end
end
