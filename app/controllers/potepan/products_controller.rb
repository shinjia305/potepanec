class Potepan::ProductsController < ApplicationController
  MAX_NUMBER_OF_DISPLAY = 4
  def show
    @product          = Spree::Product.find(params[:id])
    @taxon            = @product.taxons.first
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).
      limit(MAX_NUMBER_OF_DISPLAY)
  end
end
