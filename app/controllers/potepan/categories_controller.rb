class Potepan::CategoriesController < ApplicationController
  def show
    @taxonomies = Spree::Taxonomy.all.includes(:taxons)
    @taxon      = Spree::Taxon.find(params[:id])
    @products   = Spree::Product.in_taxon(@taxon).includes(master: [:default_price, :images])
  end
end
